#!/bin/bash

# Check if SUITE_XML_FILES is set, if so pass it to Maven
if [ -z "$SUITE_XML_FILES" ]; then
  echo "SUITE_XML_FILES is not set. Using default test configuration."
  mvn clean test
else
  echo "Running tests with the specified suite XML files: $SUITE_XML_FILES"
  mvn clean test -Dsurefire.suiteXmlFiles=$SUITE_XML_FILES

