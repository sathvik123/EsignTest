package TestCase;

import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import Pages.Addrecipients;
import Pages.Alldocuments;
import Pages.Dashboard;
import Pages.DocumentsScenarios;
import Pages.DocumentsScenarios1;
import Pages.Dragdrop;
import Pages.FormTemplate;
import Pages.Formtemplates_globalvariable;
import Pages.Login;
import Pages.MethodActions;
import Pages.RecevierSide;
import Pages.RecevierSide1;
import Pages.ReviewaNdSend;
import Pages.Signup;
import Pages.TemplateScenarios;
import Pages.Templatespage;
import Pages.Uploaddocuments;
import Pages.Testforms;
import Reports.TestNGExtentReport;
import Reports.Testlistner;
import TestBase.testCaseBase;

@Listeners({ TestNGExtentReport.class, Testlistner.class })
public class TestcaseForms extends testCaseBase {
	Login login;
	Testforms testforms;
	DocumentsScenarios1 documentsScenarios1;
	Formtemplates_globalvariable formtemplates_globalvariable;
	RecevierSide1 recevierSide1;
	DocumentsScenarios documentsScenarios;

	public void setupDriver() {
		// driver = new ChromeDriver(getChromeOptions());
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://preprod.esigns.io/documents/upload");

	}

	@BeforeMethod
	public void Start() throws Exception {
		setupDriver();
		login = new Login(driver);
		testforms = new Testforms(driver);
		documentsScenarios1 = new DocumentsScenarios1(driver);
		formtemplates_globalvariable = new Formtemplates_globalvariable(driver);
		recevierSide1 = new RecevierSide1(driver);
		documentsScenarios = new DocumentsScenarios(driver);
		login.SigninprodMeghana();
		MethodActions.Loadingmask();

	}

	private int getRandomNumber() {
		Random random = new Random();
		return random.nextInt(1000);
	}

	@Test
	public void validW9() throws Exception {
		String originalFilePath = System.getProperty("user.dir") + "/src/test/resources/docs/W9 (5).pdf";
		String downloadedFilePath = System.getProperty("user.home") + "/Downloads/W9 (5).pdf";

		MethodActions.Loadingmask();
		documentsScenarios1.uploadFileWithSendKeys(originalFilePath);
		testforms.next();
		formtemplates_globalvariable.Recipient(1, "Receiver", "jeevithapatnana200@outlook.com", " SIGNER ");
		testforms.addfields();
		MethodActions.Loadingmask();
		// testforms.checkbox();
		testforms.enterData("(//div[contains(@class,'field-textarea textarea-nowrap')]/following-sibling::div)[3]",
				"HGT");
		testforms.enterData("(//div[contains(@class,'field-textarea textarea-nowrap')]/following-sibling::div)[2]",
				"Kodefast");
		testforms.enterData("(//div[contains(@class,'field-textarea textarea-nowrap')]/following-sibling::div)[4]",
				"Test");
		testforms.enterData("(//div[contains(@class,'field-textarea textarea-nowrap')]/following-sibling::div)[8]",
				"533101");
		testforms.enterData("(//div[contains(@class,'field-textarea textarea-nowrap')]/following-sibling::div)[9]",
				"15698");
		testforms.enterData("//div[contains(@class,'field-textarea textare-background')]/following-sibling::div[1]",
				"Prj-Rjy");
		testforms.enterData("//div[@data-text='SINGLE_LINE_TEXT10']", "321");
		testforms.enterData("//div[@data-text='SINGLE_LINE_TEXT11']", "65");
		testforms.enterData("//div[@data-text='SINGLE_LINE_TEXT12']", "78");
		testforms.enterData("//div[@data-text='SINGLE_LINE_TEXT13']", "98");
		testforms.enterData("//div[@data-text='SINGLE_LINE_TEXT14']", "258654");

		testforms.RecieverField(2, -145, 10, 295, 10);
		testforms.send("Test");
		testforms.compare(originalFilePath, downloadedFilePath);

	}

	@Test
	public void validi9Form() throws Exception {
		String originalFilePath = System.getProperty("user.dir") + "/src/test/resources/docs/i-9 Form.pdf";
		String downloadedFilePath = System.getProperty("user.home") + "/Downloads/i-9 Form.pdf";
		String keys = "Test" + getRandomNumber();
		MethodActions.Loadingmask();
		documentsScenarios1.uploadFileWithSendKeys(originalFilePath);
		testforms.next();
		formtemplates_globalvariable.Recipient(1, "Receiver", "jeevithapatnana200@outlook.com", " SIGNER ");
		testforms.addfields();
		MethodActions.Loadingmask();
		testforms.pages(3);
		MethodActions.scrollIntoView(By.xpath("//div[@data-text='SINGLE_LINE_TEXT49']"));
		testforms.RecieverField(2, -300, -80, 0, -80);
		testforms.send(keys);
		// testforms.otheroptions("Download");
		testforms.compare(originalFilePath, downloadedFilePath);
		recevierSide1.Outlooklogin("jeevithapatnana200@outlook.com", "Meghana@123");
		testforms.clickemail(keys);
		recevierSide1.reviewandSign("Review & sign");
		testforms.verify();
		testforms.download(1);
		MethodActions.Loadingmask();
		testforms.compare(originalFilePath, downloadedFilePath);

	}

	@Test
	public void validattachments() throws Exception {
		String keys = "Test" + getRandomNumber();
		String Docname = "BlankDoc" + getRandomNumber();
		documentsScenarios.Blankdocloginaccount(Docname);
		formtemplates_globalvariable.Recipient(1, "Receiver", "jeevithapatnana200@outlook.com", " SIGNER ");
		MethodActions.Loadingmask();
		testforms.selectfields("Fillable Fields For");
		testforms.Fields(2, -300, -80, 0, -80, "requestEle");
		testforms.send(keys);
		recevierSide1.Outlooklogin("jeevithapatnana200@outlook.com", "Meghana@123");
		testforms.clickemail(keys);
		recevierSide1.reviewandSign("Review & sign");

	}
}
