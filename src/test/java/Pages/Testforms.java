package Pages;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.time.Duration;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.apache.pdfbox.pdmodel.interactive.form.PDField;
import org.apache.pdfbox.text.PDFTextStripper;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;

public class Testforms {

	WebDriver driver;
	MethodActions methodActions;

	public Testforms(WebDriver driver) {

		this.driver = driver;
		methodActions = new MethodActions(driver);

	}

	public void next() throws Exception {
		Thread.sleep(10000);
		MethodActions.waitEle(By.xpath("//span[text()='Next']"));

	}

	public void addfields() throws Exception {

		Thread.sleep(1000);
		try {
			MethodActions.switchToNewWindow();
			MethodActions.waitEle(By.xpath("//span[normalize-space()='Add fields']"));

		} catch (Exception e) {

		}
	}

	public void checkbox() throws Exception {
		Thread.sleep(1000);
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(30));

		for (int i = 3; i <= 9; i++) {
			WebElement checkbox = wait
					.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@type='checkbox'])[" + i + "]")));

			checkbox.click();

		}

		Thread.sleep(2000);

	}

//	public void enterData() throws Exception {
//		Thread.sleep(1000);
//		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
//
//		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(
//				By.xpath("(//div[contains(@class,'field-textarea textarea-nowrap')]/following-sibling::div)[1]")));
//
//		JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
//
//		String script = "var element = arguments[0];" + "element.dispatchEvent(new MouseEvent('dblclick', {"
//				+ "  bubbles: true," + "  cancelable: true," + "  view: window" + "}));";
//
//		jsExecutor.executeScript(script, element);
//
//		Thread.sleep(1000);
//		MethodActions.sendKeysToElement(By.xpath("(//label[normalize-space(text())='Value']/following::input)[1]"),
//				" HGT");
//		System.out.println("Action completed successfully!");
//
//		MethodActions.waitEle(By.xpath("(//button[@class='el-button el-button--primary']//span)[2]"));
//
//	}

	public void enterData(String elementXPath, String inputValue) throws Exception {
		Thread.sleep(1000);

		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(elementXPath)));

		JavascriptExecutor jsExecutor = (JavascriptExecutor) driver;
		jsExecutor.executeScript("arguments[0].scrollIntoView(true);", element);

		// Double-click using JavaScript
		String script = "var element = arguments[0];" + "element.dispatchEvent(new MouseEvent('dblclick', {"
				+ "  bubbles: true," + "  cancelable: true," + "  view: window" + "}));";
		jsExecutor.executeScript(script, element);

		Thread.sleep(1000);

		MethodActions.sendKeysToElement(By.xpath("(//label[normalize-space(text())='Value']/following::input)[1]"),
				inputValue);

		System.out.println("Data Entered  successfully!");

		MethodActions.waitEle(By.xpath("(//button[@class='el-button el-button--primary']//span)[2]"));
	}

	public void select() throws Exception {
		WebElement recipientInput = driver.findElement(By.xpath("//input[@placeholder='Select a Recipient']"));

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView(true);", recipientInput);

		Thread.sleep(2000);

		recipientInput.click();

		Thread.sleep(2000);

		MethodActions.waitEle(By.xpath("//div[@x-placement]//li[2]"));
	}

	public void RecieverField(int n, int x1, int y1, int x2, int y2) throws Exception {
		Thread.sleep(10000);
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofMinutes(1));
		WebElement element = wait
				.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@placeholder='Select a Recipient']")));

		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);

		wait.until(ExpectedConditions.elementToBeClickable(element));

		((JavascriptExecutor) driver).executeScript("arguments[0].click();", element);

		Thread.sleep(5000);

		MethodActions.waitEle(By.xpath(String.format("//div[@x-placement]//li[%d]", n)));

		Thread.sleep(5000);
		Actions actions1 = new Actions(driver);

		WebElement targetElement = driver.findElement(By.xpath("//*[@id=\"svg\"]"));

		WebElement fullNameElement = driver.findElement(By.xpath("//button[@id='signatureEle']"));
		waitAndClick(actions1, fullNameElement, targetElement, x1, y1);
		System.out.println("FullName Drag and Drop Done");
		Thread.sleep(5000);

		WebElement AddressElement = driver.findElement(By.xpath("//button[@id='dateSingedEle']"));

		waitAndClick(actions1, AddressElement, targetElement, x2, y2);
		System.out.println("dateSinged Element Drag and Drop Done");

//		WebElement TitleElement = driver.findElement(By.xpath("//button[@id='titleEle']"));
//		waitAndClick(actions1, TitleElement, targetElement, x3, y3);
//		System.out.println("Title Element Drag and Drop Done");
//
//		WebElement fullNameElement1 = driver.findElement(By.xpath("//button[@id='fullNameEle']"));
//		waitAndClick(actions1, fullNameElement1, targetElement, x4, y4);
//		System.out.println("FullName Drag and Drop Done");
//		Thread.sleep(5000);
	}

	public void waitAndClick(Actions actions, WebElement sourceElement, WebElement targetElement, int xOffset,
			int yOffset) {
		actions.clickAndHold(sourceElement).moveToElement(targetElement, xOffset, yOffset).release().build().perform();
	}

	public void send(String Input) throws Exception {
		Thread.sleep(1000);
		MethodActions.waitEle(By.xpath("//span[normalize-space(text())='Send Document']"));

//		MethodActions.sendKeysToElement(By.xpath("//textarea[@placeholder='Subject']"), "Test");
		Thread.sleep(10000);
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(40));
		WebElement keys = driver.findElement(By.xpath("//textarea[@placeholder='Subject']"));
		keys.sendKeys(Input);

		MethodActions.sendKeysToElement(By.xpath("//textarea[@placeholder='Type your text here']"),
				"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n"
						+ "");

		MethodActions.Javascriptclick(By.xpath("//span[normalize-space(text())='Send Document']"));

		Thread.sleep(1000);
		MethodActions.waitEle(By.xpath("//span[normalize-space()='View Document']"));

		Thread.sleep(1000);
		MethodActions.waitEle(By.xpath("//span[normalize-space()='FINISH']"));

		Thread.sleep(1000);
		MethodActions.waitEle(By.xpath("//button[contains(.,'OK')]"));

	}

	public void otheroptions(String name, int num) throws Exception {
		Thread.sleep(10000);
		MethodActions.waitEle(By.xpath("//span[normalize-space(text())='Other Options']"));

		MethodActions.waitEle(By.xpath(String.format("//ul[@x-placement]//li[normalize-space(text())='%s']", name)));

	}

	public void download(int num) throws Exception {
		MethodActions.Loadingmask();
		Thread.sleep(10000);
		// driver.get("https://preprod.esigns.io/emp-documents/custom/673c74f79ea8b379e7b1a53b/pp");
		MethodActions.Loadingmask();
		MethodActions.waitEle(By.xpath("//span[normalize-space(text())='Download']"));
		Thread.sleep(1000);
		MethodActions.waitEle(By.xpath(String.format("//ul[@x-placement]//li[%d]", num)));

	}

	public void clickemail(String email) throws Exception {
		Thread.sleep(10000);

		MethodActions.waitEle(By.xpath(String.format("//span[normalize-space(text())='%s']", email)));
		Thread.sleep(1000);
		// MethodActions.waitEle(By.xpath("//a[normalize-space(text())='View
		// Document']"));
	}

	public void reviewandSign(String s) throws Exception {
		Thread.sleep(10000);
		WebElement copy = driver.findElement(By.xpath("//td[@class=\"x_featured-story__content-inner\"]//span"));

		Actions action2 = new Actions(driver);
		action2.doubleClick(copy).build().perform();
		Thread.sleep(10000);
		Actions actions = new Actions(driver);
		actions.moveToElement(copy).keyDown(Keys.CONTROL) // Press Ctrl key
				.sendKeys("c") // Press C key
				.keyUp(Keys.CONTROL) // Release Ctrl key
				.build().perform();

		Set<String> windowHandles1 = driver.getWindowHandles();
		for (String handle : windowHandles1) {
			driver.switchTo().window(handle);
		}
		Thread.sleep(10000);
		String Parentwindowid1 = driver.getWindowHandle();

		MethodActions.waitEle(By.xpath(String.format("//a[text()='%s']", s)));

		Thread.sleep(10000);
		Set<String> allwindowhandles1 = driver.getWindowHandles();
		for (String childwindow : allwindowhandles1) {
			if (!childwindow.endsWith(Parentwindowid1)) {
				driver.switchTo().window(Parentwindowid1).close();
				driver.switchTo().window(childwindow);

				Thread.sleep(10000);
				MethodActions.waitEle(By.xpath("//input[@placeholder=\"Enter 6 letters code\"]"));
				WebElement paste = driver.findElement(By.xpath("//input[@placeholder=\"Enter 6 letters code\"]"));

				actions.moveToElement(paste).keyDown(Keys.CONTROL) // Press Ctrl key
						.sendKeys("v") // Press v key
						.keyUp(Keys.CONTROL) // Release Ctrl key
						.build().perform();
				MethodActions.waitEle(By.xpath("//div[@class=\"el-form-item__content\"]//span[text()=\"Submit\"]"));
				System.out.println("Entered 6 letter code");
			}
		}

		try {
			Thread.sleep(10000);
			MethodActions.waitEle(By.xpath("//span[@class=\"el-checkbox__inner\"]"));
			MethodActions.waitEle(By.xpath("//span[text()=\"Continue\"]"));

		} catch (Exception e) {
			Thread.sleep(10000);
			MethodActions.waitEle(By.xpath("//span[normalize-space()='NEXT FIELD']"));
		}
	}

	public void pages(int p) throws Exception {

		Thread.sleep(1000);

		MethodActions.waitEle(By.xpath(String.format("//canvas[@id='0_canvas_page_%d']", p)));

	}

	public void verify() throws Exception {
		Thread.sleep(10000);
		MethodActions.waitEle(By.xpath("//span[normalize-space()='Verify & Save Secure Signature']"));
		MethodActions.waitEle(By.xpath("//span[normalize-space()='FINISH']"));
		MethodActions.Loadingmask();
	}

	public void scroll() {
		Actions actions = new Actions(driver);

		actions.moveByOffset(0, 200).click().perform();

	}

	public void scale() {
		WebElement signatureField = driver.findElement(By.xpath(""));

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].style.transform = 'scale(1.5)';", signatureField);

		js.executeScript("arguments[0].style.transformOrigin = '0 0';", signatureField);

	}

	public static String extractTextFromPDF(String pdfPath) throws IOException {
		PDDocument document = PDDocument.load(new File(pdfPath));
		PDFTextStripper stripper = new PDFTextStripper();
		String text = stripper.getText(document);
		document.close();
		return text;
	}

	public static void printDifferences(String text1, String text2) {

		String[] lines1 = text1.split("\n");
		String[] lines2 = text2.split("\n");

		lines1 = normalizeText(lines1);
		lines2 = normalizeText(lines2);

		int maxLength = Math.max(lines1.length, lines2.length);

		for (int i = 0; i < maxLength; i++) {
			if (i >= lines1.length) {
				System.out.println("Line " + (i + 1) + " (Downloaded PDF): " + lines2[i]);
			} else if (i >= lines2.length) {
				System.out.println("Line " + (i + 1) + " (Local PDF): " + lines1[i]);
			} else if (!lines1[i].equals(lines2[i])) {
				System.out.println("Difference at Line " + (i + 1) + ":");
				System.out.println("Local PDF: " + lines1[i]);
				System.out.println("Downloaded PDF: " + lines2[i]);
				System.out.println("---------");
			}
		}
	}

	private static String[] normalizeText(String[] lines) {
		for (int i = 0; i < lines.length; i++) {
			lines[i] = lines[i].replaceAll("\\s+", " ").trim();
		}
		return lines;
	}

	public void compare(String originalFilePath, String downloadedFilePath) {

		try {

			String originalText = extractTextFromPDF(originalFilePath);
			String downloadedText = extractTextFromPDF(downloadedFilePath);

			if (originalText.equals(downloadedText)) {
				System.out.println("The PDFs are identical in text.");
			} else {
				System.err.println("The PDFs have differences in text.");
			}

			printDifferences(originalText, downloadedText);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void selectfields(String field) throws Exception {
		Thread.sleep(10000);
		MethodActions.Loadingmask();
		// Fillable Fields For
		String formattedXpath = String.format("//div[normalize-space(text())='%s']", field);
		WebElement element = driver.findElement(By.xpath(formattedXpath));

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].scrollIntoView(true);", element);

	}

	public void Fields(int n, int x1, int y1, int x2, int y2, String button) throws Exception {
		Thread.sleep(10000);

		WebDriverWait wait = new WebDriverWait(driver, Duration.ofMinutes(1));
		WebElement element = wait
				.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@placeholder='Select a Recipient']")));

		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);

		wait.until(ExpectedConditions.elementToBeClickable(element));

		((JavascriptExecutor) driver).executeScript("arguments[0].click();", element);

		Thread.sleep(5000);

		MethodActions.waitEle(By.xpath(String.format("//div[@x-placement]//li[%d]", n)));

		Thread.sleep(5000);
		Actions actions1 = new Actions(driver);

		WebElement targetElement = driver.findElement(By.xpath("//*[@id=\"svg\"]"));

		WebElement fullNameElement = driver.findElement(By.xpath(String.format("//button[@id='%s']", button)));
		waitAndClick(actions1, fullNameElement, targetElement, x1, y1);
		System.out.println("attachement Drag and Drop Done");
		Thread.sleep(5000);

	}

}
