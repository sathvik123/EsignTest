FROM openjdk:17-slim

# Set environment variables
ENV DEBIAN_FRONTEND=noninteractive
ENV CHROMEDRIVER_ARGS="--no-sandbox --headless --disable-dev-shm-usage"
ENV CHROME_ARGS="--no-sandbox --headless --disable-gpu --disable-dev-shm-usage"

# Install dependencies
RUN apt-get update && apt-get install -y \
    wget \
    curl \
    unzip \
    gnupg \
    apt-transport-https \
    ca-certificates \
    maven \
    && rm -rf /var/lib/apt/lists/*

# Install Google Chrome
RUN mkdir -p /etc/apt/keyrings \
    && wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | gpg --dearmor -o /etc/apt/keyrings/google-chrome.gpg \
    && echo "deb [arch=amd64 signed-by=/etc/apt/keyrings/google-chrome.gpg] http://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/google-chrome.list \
    && apt-get update \
    && apt-get install -y google-chrome-stable \
    && google-chrome-stable --version \
    && rm -rf /var/lib/apt/lists/*

# Install ChromeDriver
RUN CHROMEDRIVER_VERSION="131.0.6778.204" \
    && wget -q "https://storage.googleapis.com/chrome-for-testing-public/${CHROMEDRIVER_VERSION}/linux64/chromedriver-linux64.zip" -O /tmp/chromedriver.zip \
    && unzip /tmp/chromedriver.zip -d /tmp/ \
    && mv /tmp/chromedriver-linux64/chromedriver /usr/local/bin/ \
    && rm -rf /tmp/chromedriver.zip /tmp/chromedriver-linux64 \
    && chmod +x /usr/local/bin/chromedriver

# Set the working directory
WORKDIR /app

# Copy the project files
COPY pom.xml .
COPY src /app/src
COPY testNGXML /app/testNGXML
COPY target/chaintest /app/target/chaintest


# Run mvn test directly
CMD ["sh", "-c", "mvn clean test -Dsurefire.suiteXmlFiles=${SUITE_XML_FILES}"]
